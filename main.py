import tkinter as tk
from tkinter import ttk


class TestFrame(tk.Frame):
    def __init__(self):
        super().__init__()
        self.dimension = (
        'lidar-calibration','figure8','Run')
        self.option_var = tk.IntVar(self)
        self.option_var1 = tk.IntVar(self)
        self.option_var2 = tk.IntVar(self)
        self.option_var3 = tk.IntVar(self)
        self.option_var4 = tk.IntVar(self)
        self.option_var5 = tk.IntVar(self)
        self.option_var6 = tk.IntVar(self)
        self.option_var7 = tk.IntVar(self)
        # self.create_widgets()
        # self.create_widgets_1()

        paddings = {'padx': 5, 'pady': 5}
        label = ttk.Label(self, text='Capture').grid(column=0, row=0, sticky=tk.W, **paddings)
        # ODD_X1
        option_menu = ttk.OptionMenu(
            self,
            self.option_var,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed)
        option_menu.grid(column=1, row=0, sticky=tk.W, **paddings)
        self.output_label = ttk.Label(self, foreground='red')
        self.output_label.grid(column=0, row=1, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Sub-capture:').grid(column=3, row=0, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var1,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed1)
        option_menu.grid(column=4, row=0, sticky=tk.W, **paddings)
        self.output_label1 = ttk.Label(self, foreground='red')
        self.output_label1.grid(column=4, row=1, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Run number:').grid(column=0, row=3, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var2,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed2)
        option_menu.grid(column=1, row=3, sticky=tk.W, **paddings)
        self.output_label2 = ttk.Label(self, foreground='red')
        self.output_label2.grid(column=0, row=4, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Date:').grid(column=3, row=3, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var3,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed3)
        option_menu.grid(column=4, row=3, sticky=tk.W, **paddings)
        self.output_label3 = ttk.Label(self, foreground='red')
        self.output_label3.grid(column=4, row=4, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Time of the day:').grid(column=0, row=5, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var4,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed4)
        option_menu.grid(column=1,row=5, sticky=tk.W, **paddings)
        self.output_label4 = ttk.Label(self, foreground='red')
        self.output_label4.grid(column=8,row=1, sticky=tk.W, **paddings)

        paddings = {'padx': 3, 'pady': 3}
        ttk.Label(self,text='Algolux Copyright').grid(column=0, row=17, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var5,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed5)
        option_menu.grid(column=1, row=10, sticky=tk.W, **paddings)






    def option_changed(self, *args):
        self.output_label['text'] = f'You selected: {self.option_var.get()}'

    def option_changed1(self, *args):
        self.output_label1['text'] = f'You selected: {self.option_var1.get()}'

    def option_changed2(self, *args):
        self.output_label2['text'] = f'You selected: {self.option_var2.get()}'

    def option_changed3(self, *args):
        self.output_label3['text'] = f'You selected: {self.option_var3.get()}'

    def option_changed4(self, *args):
        self.output_label4['text'] = f'You selected: {self.option_var4.get()}'

    def option_changed5(self, *args):
        self.output_label5['text'] = f'You selected: {self.option_var5.get()}'


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.geometry("600x400")
        self.title('Renaming_Tool')
        for r in range(5):
            self.rowconfigure(r, weight=1)
        for c in range(8):
            self.columnconfigure(c, weight=1)

        self.testFrame1 = TestFrame()
        self.testFrame1.grid(row=0, column=0, rowspan=3, columnspan=3, sticky='nsew')


App().mainloop()
