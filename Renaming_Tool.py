import tkinter as tk
from tkinter import ttk
import os, fnmatch

class TestFrame(tk.Frame):
    def __init__(self):
        super().__init__()
        super().__init__()
        self.dimension = *('151', '152', '153'),
        self.dimension1 = *('lidar-calibration', 'Highway', 'Atwater', 'Du_Parc_Centre'),
        self.dimension2 = *('1', '2', '3', '4', '5', '6', '7', '8'),
        self.dimension3 = *('20211026', '20211027', '20211028'),
        self.dimension4 = *('Morning', 'Afternoon', 'Night'),
        self.dimension5 = *('Rename_file',''),
        
        self.capture = ''
        self.sub_capture = ''
        self.run_number = ''
        self.date = ''
        self.time_Date = ''

        self.option_var = tk.IntVar(self)
        self.option_var1 = tk.StringVar(self)
        self.option_var2 = tk.IntVar(self)
        self.option_var3 = tk.IntVar(self)
        self.option_var4 = tk.StringVar(self)
        self.option_var5 = tk.StringVar(self)

        # self.create_widgets()
        # self.create_widgets_1()

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Capture').grid(column=0, row=0, sticky=tk.W, **paddings)
        # ODD_X1
        option_menu = ttk.OptionMenu(
            self,
            self.option_var,
            self.dimension[0],
            *self.dimension,
            command=self.option_changed)
        option_menu.grid(column=1, row=0, sticky=tk.W, **paddings)
        self.output_label = ttk.Label(self, foreground='red')
        self.output_label.grid(column=0, row=1, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Sub-capture:').grid(column=3, row=0, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var1,
            self.dimension1[0],
            *self.dimension1,
            command=self.option_changed1)
        option_menu.grid(column=4, row=0, sticky=tk.W, **paddings)
        self.output_label1 = ttk.Label(self, foreground='red')
        self.output_label1.grid(column=4, row=1, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Run number:').grid(column=0, row=3, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var2,
            self.dimension2[0],
            *self.dimension2,
            command=self.option_changed2)
        option_menu.grid(column=1, row=3, sticky=tk.W, **paddings)
        self.output_label2 = ttk.Label(self, foreground='red')
        self.output_label2.grid(column=0, row=4, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Date:').grid(column=3, row=3, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var3,
            self.dimension3[0],
            *self.dimension3,
            command=self.option_changed3)
        option_menu.grid(column=4, row=3, sticky=tk.W, **paddings)
        self.output_label3 = ttk.Label(self, foreground='red')
        self.output_label3.grid(column=4, row=4, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        ttk.Label(self, text='Time of the day:').grid(column=0, row=5, sticky=tk.W, **paddings)
        option_menu = ttk.OptionMenu(
            self,
            self.option_var4,
            self.dimension4[0],
            *self.dimension4,
            command=self.option_changed4)
        option_menu.grid(column=1, row=5, sticky=tk.W, **paddings)
        self.output_label4 = ttk.Label(self, foreground='red')
        self.output_label4.grid(column=0, row=8, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        option_menu = ttk.OptionMenu(
            self,
            self.option_var5,
            self.dimension5[0],
            *self.dimension5,
            command=self.concatfunc)
        option_menu.grid(column=4, row=8, sticky=tk.W, **paddings)
        self.output_label5 = ttk.Label(self, foreground='red')
        self.output_label5.grid(column=10, row=15, sticky=tk.W, **paddings)

        paddings = {'padx': 5, 'pady': 5}
        option_menu.grid(column=4, row=8, sticky=tk.W, **paddings)
        self.output_label6 = ttk.Label(self, foreground='red')
        self.output_label6.grid(column=10, row=15, sticky=tk.W, **paddings)



    def option_changed(self, *args):
        self.output_label['text'] = f'{self.option_var.get()}'
        self.capture = self.option_var.get()
        return self.capture

    def option_changed1(self, *args):
        self.output_label1['text'] = f'{self.option_var1.get()}'
        self.sub_capture = self.option_var1.get()

    def option_changed2(self, *args):
        self.output_label2['text'] = f'{self.option_var2.get()}'
        self.run_number = self.option_var2.get()

    def option_changed3(self, *args):
        self.output_label3['text'] = f'{self.option_var3.get()}'
        self.date = self.option_var3.get()

    def option_changed4(self, *args):
        self.output_label4['text'] = f'{self.option_var4.get()}'
        self.time_Date = self.option_var4.get()

    def concatfunc(self, *args):
        self.output_label5['text'] = (
                    "File will be renamed as:" "CAPT-" + str(self.capture) + "_" + str(self.sub_capture) + "_"
                    + str(self.date) + "_RUN_" + str(self.run_number) + "_" + str(self.time_Date))

    def concatfunc_export(self, *args):
        self.concatfunc_export['text'] = ("CAPT-" + str(self.capture) + "_" + str(self.sub_capture) + "_"
                                          + str(self.date) + "_RUN_" + str(self.run_number) + "_" + str(self.time_Date))
        print(self.concatfunc_export)
#
# files_to_rename = ['Comforter.png']
# folder = r"/home/preyash.shah/Pictures/UPS"
#
# for file in os.listdir(folder):
#     if file in files_to_rename:
#         old_name = os.path.join(folder, file)
#
#         only_name = os.path.splitext(file)[0]
#
#         new_input_name = only_name +
#         print("File renamed successfully")
#     else:
#
#         print("File doesn't exits")
#
#         new_name = os.path.join(folder, new_input_name)
#
#         os.rename(old_name, new_name)
#
# res = os.listdir(folder)
# print(res)
#
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.geometry("1200x600")
        self.title('Renaming_Tool')
        for r in range(5):
            self.rowconfigure(r, weight=1)
        for c in range(8):
            self.columnconfigure(c, weight=1)

        self.testFrame1 = TestFrame()
        self.testFrame1.grid(row=0, column=0, rowspan=3, columnspan=3, sticky='nsew')



App().mainloop()
